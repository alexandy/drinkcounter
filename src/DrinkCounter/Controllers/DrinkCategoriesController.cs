using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DrinkCounter.Models;

namespace DrinkCounter.Controllers
{
    [Produces("application/json")]
    [Route("api/DrinkCategories")]
    public class DrinkCategoriesController : Controller
    {
        private drinkdataContext _context;

        public DrinkCategoriesController(drinkdataContext context)
        {
            _context = context;
        }

        // GET: api/DrinkCategories
        [HttpGet]
        public IEnumerable<DrinkCategories> GetDrinkCategories()
        {
            return _context.DrinkCategories;
        }

        // GET: api/DrinkCategories/5
        [HttpGet("{id}", Name = "GetDrinkCategories")]
        public async Task<IActionResult> GetDrinkCategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkCategories drinkCategories = await _context.DrinkCategories.SingleAsync(m => m.Id == id);

            if (drinkCategories == null)
            {
                return HttpNotFound();
            }

            return Ok(drinkCategories);
        }

        // PUT: api/DrinkCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDrinkCategories([FromRoute] int id, [FromBody] DrinkCategories drinkCategories)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != drinkCategories.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(drinkCategories).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrinkCategoriesExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/DrinkCategories
        [HttpPost]
        public async Task<IActionResult> PostDrinkCategories([FromBody] DrinkCategories drinkCategories)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.DrinkCategories.Add(drinkCategories);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DrinkCategoriesExists(drinkCategories.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetDrinkCategories", new { id = drinkCategories.Id }, drinkCategories);
        }

        // DELETE: api/DrinkCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDrinkCategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkCategories drinkCategories = await _context.DrinkCategories.SingleAsync(m => m.Id == id);
            if (drinkCategories == null)
            {
                return HttpNotFound();
            }

            _context.DrinkCategories.Remove(drinkCategories);
            await _context.SaveChangesAsync();

            return Ok(drinkCategories);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DrinkCategoriesExists(int id)
        {
            return _context.DrinkCategories.Count(e => e.Id == id) > 0;
        }
    }
}