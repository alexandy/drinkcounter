using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DrinkCounter.Models;

namespace DrinkCounter.Controllers
{
    [Produces("application/json")]
    [Route("api/DrinkCounts")]
    public class DrinkCountsController : Controller
    {
        private drinkdataContext _context;

        public DrinkCountsController(drinkdataContext context)
        {
            _context = context;
        }

        // GET: api/DrinkCounts
        [HttpGet]
        public IEnumerable<DrinkCount> GetDrinkCount()
        {
            return _context.DrinkCount;
        }

        // GET: api/DrinkCounts/5
        [HttpGet("{id}", Name = "GetDrinkCount")]
        public async Task<IActionResult> GetDrinkCount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkCount drinkCount = await _context.DrinkCount.SingleAsync(m => m.Id == id);

            if (drinkCount == null)
            {
                return HttpNotFound();
            }

            return Ok(drinkCount);
        }

        // PUT: api/DrinkCounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDrinkCount([FromRoute] int id, [FromBody] DrinkCount drinkCount)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != drinkCount.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(drinkCount).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrinkCountExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/DrinkCounts
        [HttpPost]
        public async Task<IActionResult> PostDrinkCount([FromBody] DrinkCount drinkCount)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.DrinkCount.Add(drinkCount);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DrinkCountExists(drinkCount.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetDrinkCount", new { id = drinkCount.Id }, drinkCount);
        }

        // DELETE: api/DrinkCounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDrinkCount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkCount drinkCount = await _context.DrinkCount.SingleAsync(m => m.Id == id);
            if (drinkCount == null)
            {
                return HttpNotFound();
            }

            _context.DrinkCount.Remove(drinkCount);
            await _context.SaveChangesAsync();

            return Ok(drinkCount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DrinkCountExists(int id)
        {
            return _context.DrinkCount.Count(e => e.Id == id) > 0;
        }
    }
}