using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DrinkCounter.Models;

namespace DrinkCounter.Controllers
{
    [Produces("application/json")]
    [Route("api/DrinkTypes")]
    public class DrinkTypesController : Controller
    {
        private drinkdataContext _context;

        public DrinkTypesController(drinkdataContext context)
        {
            _context = context;
        }

        // GET: api/DrinkTypes
        [HttpGet]
        public IEnumerable<DrinkTypes> GetDrinkTypes()
        {
            return _context.DrinkTypes;
        }

        // GET: api/DrinkTypes/5
        [HttpGet("{id}", Name = "GetDrinkTypes")]
        public async Task<IActionResult> GetDrinkTypes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkTypes drinkTypes = await _context.DrinkTypes.SingleAsync(m => m.Id == id);

            if (drinkTypes == null)
            {
                return HttpNotFound();
            }

            return Ok(drinkTypes);
        }

        // PUT: api/DrinkTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDrinkTypes([FromRoute] int id, [FromBody] DrinkTypes drinkTypes)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != drinkTypes.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(drinkTypes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrinkTypesExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/DrinkTypes
        [HttpPost]
        public async Task<IActionResult> PostDrinkTypes([FromBody] DrinkTypes drinkTypes)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.DrinkTypes.Add(drinkTypes);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DrinkTypesExists(drinkTypes.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetDrinkTypes", new { id = drinkTypes.Id }, drinkTypes);
        }

        // DELETE: api/DrinkTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDrinkTypes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            DrinkTypes drinkTypes = await _context.DrinkTypes.SingleAsync(m => m.Id == id);
            if (drinkTypes == null)
            {
                return HttpNotFound();
            }

            _context.DrinkTypes.Remove(drinkTypes);
            await _context.SaveChangesAsync();

            return Ok(drinkTypes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DrinkTypesExists(int id)
        {
            return _context.DrinkTypes.Count(e => e.Id == id) > 0;
        }
    }
}