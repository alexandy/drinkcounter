using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DrinkCounter.Models;

namespace DrinkCounter.Controllers
{
    [Produces("application/json")]
    [Route("api/TeamMembers")]
    public class TeamMembersController : Controller
    {
        private drinkdataContext _context;

        public TeamMembersController(drinkdataContext context)
        {
            _context = context;
        }

        // GET: api/TeamMembers
        [HttpGet]
        public IEnumerable<TeamMember> GetTeamMember()
        {
            return _context.TeamMember;
        }

        // GET: api/TeamMembers/5
        [HttpGet("{id}", Name = "GetTeamMember")]
        public async Task<IActionResult> GetTeamMember([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            TeamMember teamMember = await _context.TeamMember.SingleAsync(m => m.AspNetUsers_Id == id);

            if (teamMember == null)
            {
                return HttpNotFound();
            }

            return Ok(teamMember);
        }

        // PUT: api/TeamMembers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeamMember([FromRoute] string id, [FromBody] TeamMember teamMember)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != teamMember.AspNetUsers_Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(teamMember).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamMemberExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/TeamMembers
        [HttpPost]
        public async Task<IActionResult> PostTeamMember([FromBody] TeamMember teamMember)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.TeamMember.Add(teamMember);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TeamMemberExists(teamMember.AspNetUsers_Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetTeamMember", new { id = teamMember.AspNetUsers_Id }, teamMember);
        }

        // DELETE: api/TeamMembers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeamMember([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            TeamMember teamMember = await _context.TeamMember.SingleAsync(m => m.AspNetUsers_Id == id);
            if (teamMember == null)
            {
                return HttpNotFound();
            }

            _context.TeamMember.Remove(teamMember);
            await _context.SaveChangesAsync();

            return Ok(teamMember);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TeamMemberExists(string id)
        {
            return _context.TeamMember.Count(e => e.AspNetUsers_Id == id) > 0;
        }
    }
}