using System;
using System.Collections.Generic;

namespace DrinkCounter.Models
{
    public partial class DrinkCategories
    {
        public DrinkCategories()
        {
            DrinkTypes = new HashSet<DrinkTypes>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DrinkTypes> DrinkTypes { get; set; }
    }
}
