using System;
using System.Collections.Generic;

namespace DrinkCounter.Models
{
    public partial class DrinkTypes
    {
        public DrinkTypes()
        {
            DrinkCount = new HashSet<DrinkCount>();
        }

        public int Id { get; set; }
        public int? Calories { get; set; }
        public string Description { get; set; }
        public int DrinkCategories_Id { get; set; }
        public string Name { get; set; }
        public string Temperature { get; set; }

        public virtual ICollection<DrinkCount> DrinkCount { get; set; }
        public virtual DrinkCategories DrinkCategories { get; set; }
    }
}
