using System;
using System.Collections.Generic;

namespace DrinkCounter.Models
{
    public partial class DrinkCount
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string AspNetUsers_Id { get; set; }
        public DateTime Date { get; set; }
        public int DrinkTypes_Id { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual DrinkTypes DrinkTypes { get; set; }
    }
}
