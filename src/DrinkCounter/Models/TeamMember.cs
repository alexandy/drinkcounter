using System;
using System.Collections.Generic;

namespace DrinkCounter.Models
{
    public partial class TeamMember
    {
        public string AspNetUsers_Id { get; set; }
        public int team_id { get; set; }
        public bool? activated { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual Team team { get; set; }
    }
}
