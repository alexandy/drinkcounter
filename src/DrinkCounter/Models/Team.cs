using System;
using System.Collections.Generic;

namespace DrinkCounter.Models
{
    public partial class Team
    {
        public Team()
        {
            TeamMember = new HashSet<TeamMember>();
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TeamMember> TeamMember { get; set; }
    }
}
