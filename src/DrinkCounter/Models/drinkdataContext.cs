using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;

namespace DrinkCounter.Models
{
    public partial class drinkdataContext : DbContext
    {

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.Property(e => e.RoleId).HasMaxLength(450);

                entity.HasOne(d => d.Role).WithMany(p => p.AspNetRoleClaims).HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName).HasName("RoleNameIndex");

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.HasOne(d => d.User).WithMany(p => p.AspNetUserClaims).HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.Property(e => e.LoginProvider).HasMaxLength(450);

                entity.Property(e => e.ProviderKey).HasMaxLength(450);

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.HasOne(d => d.User).WithMany(p => p.AspNetUserLogins).HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.Property(e => e.UserId).HasMaxLength(450);

                entity.Property(e => e.RoleId).HasMaxLength(450);

                entity.HasOne(d => d.Role).WithMany(p => p.AspNetUserRoles).HasForeignKey(d => d.RoleId).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.User).WithMany(p => p.AspNetUserRoles).HasForeignKey(d => d.UserId).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail).HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName).HasName("UserNameIndex");

                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<DrinkCategories>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(256)
                    .HasColumnType("varchar");
            });

            modelBuilder.Entity<DrinkCount>(entity =>
            {
                entity.Property(e => e.Amount).HasDefaultValue(1);

                entity.Property(e => e.AspNetUsers_Id)
                    .IsRequired()
                    .HasMaxLength(450)
                    .HasColumnName("AspNetUsers.Id");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DrinkTypes_Id).HasColumnName("DrinkTypes.Id");

                entity.HasOne(d => d.AspNetUsers).WithMany(p => p.DrinkCount).HasForeignKey(d => d.AspNetUsers_Id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.DrinkTypes).WithMany(p => p.DrinkCount).HasForeignKey(d => d.DrinkTypes_Id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<DrinkTypes>(entity =>
            {
                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.DrinkCategories_Id).HasColumnName("DrinkCategories.Id");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.Temperature)
                    .HasMaxLength(1)
                    .HasColumnType("char");

                entity.HasOne(d => d.DrinkCategories).WithMany(p => p.DrinkTypes).HasForeignKey(d => d.DrinkCategories_Id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(256)
                    .HasColumnType("varchar");
            });

            modelBuilder.Entity<TeamMember>(entity =>
            {
                entity.HasKey(e => new { e.AspNetUsers_Id, e.team_id });

                entity.Property(e => e.AspNetUsers_Id)
                    .HasMaxLength(450)
                    .HasColumnName("AspNetUsers.Id");

                entity.Property(e => e.team_id).HasColumnName("team.id");

                entity.Property(e => e.activated).HasDefaultValue(true);

                entity.HasOne(d => d.AspNetUsers).WithMany(p => p.TeamMember).HasForeignKey(d => d.AspNetUsers_Id).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.team).WithMany(p => p.TeamMember).HasForeignKey(d => d.team_id).OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<UserInfo>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(450);

                entity.Property(e => e.Address).HasMaxLength(256);

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .HasColumnType("char");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.HasOne(d => d.AspNetUsers).WithOne(p => p.UserInfo).HasForeignKey<UserInfo>(d => d.Id).OnDelete(DeleteBehavior.Restrict);
            });
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<DrinkCategories> DrinkCategories { get; set; }
        public virtual DbSet<DrinkCount> DrinkCount { get; set; }
        public virtual DbSet<DrinkTypes> DrinkTypes { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<TeamMember> TeamMember { get; set; }
        public virtual DbSet<UserInfo> UserInfo { get; set; }
    }
}